<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
	
	public function __construct()
    {
        parent::__construct();
		$this->load->model("welcome_model");
    }

	function index()
	{
		$this->load->library('curl');
		
		$api_url = "https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY";

        $ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
   		curl_close($ch);

		$nifty_json 		= json_decode($output);
		$records 			= $nifty_json->records;
		$expiryDates		= $records->expiryDates;

		if(!empty($expiryDates))
		{		
			foreach($expiryDates as $exDate)
			{
				$ex_res = $this->welcome_model->check_expiry_date($exDate);
				if(empty($ex_res))
				{
					$expiry['expiry_date'] = date("Y-m-d",strtotime($exDate));
					$this->welcome_model->insert_expiry_date($expiry);
				}
			}		
		}

		$data['expiry_date'] = $this->welcome_model->get_expiry_date();

		$this->load->view("nse/index.php", $data);
	}

	function save_nifty()
	{
		$this->load->library('curl');
		
		$api_url = "https://www.nseindia.com/api/option-chain-indices?symbol=NIFTY";

        $ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
   		curl_close($ch);

		$nifty_json 		= json_decode($output);

		if(empty($nifty_json->records))
		{
			die('nifty no data'); exit();
		}

		$records 			= $nifty_json->records;
		$expiryDates		= $records->expiryDates;

		if(!empty($expiryDates))
		{		
			foreach($expiryDates as $exDate)
			{
				$ex_res = $this->welcome_model->check_expiry_date($exDate);
				if(empty($ex_res))
				{
					$expiry['expiry_date'] = date("Y-m-d",strtotime($exDate));
					$this->welcome_model->insert_expiry_date($expiry);
				}
			}		
		}
		
		$data 				= $records->data;
		$timestamp 			= $records->timestamp;
		$underlyingValue	= $records->underlyingValue;

		$uploaded_no		= $this->welcome_model->get_max_uploaded_no();
		$uploaded_no1     	= 1;
		if(!empty($uploaded_no['uploaded_no']))
		{
			$uploaded_no1 = $uploaded_no['uploaded_no'] + 1;
		}

		$snifty['type'] 				= 1;
		$snifty['r_status'] 			= 1;
		$snifty['uploaded_no'] 			= $uploaded_no1;
		$snifty['stock_price'] 			= $underlyingValue;
		$snifty['created_at'] 			= date("Y-m-d H:i:s", strtotime($timestamp));
		$this->welcome_model->insert_single_data($snifty);

		if(!empty($data))
		{

			foreach($data as $row_data)
			{

				$strikePrice 	= $row_data->strikePrice;
				$expiryDate 	= $row_data->expiryDate;

				if(!empty($row_data->CE))
				{
					$CE = $row_data->CE;
					
					$mnifty['calls_oi'] 		= $CE->openInterest;
					$mnifty['calls'] 			= $CE->changeinOpenInterest;
				}else{
					$mnifty['calls_oi'] 		= 0;
					$mnifty['calls'] 			= 0;
				}

				if(!empty($row_data->PE))
				{
					$PE = $row_data->PE;

					$mnifty['puts_oi'] 			= $PE->openInterest;
					$mnifty['puts'] 			= $PE->changeinOpenInterest;
					
				}else{
					$mnifty['puts_oi'] 			= 0;
					$mnifty['puts'] 			= 0;
				}

				$mnifty['stock_price'] 		= $strikePrice;
				$mnifty['expiry_date'] 		= date("Y-m-d", strtotime($expiryDate));
				$mnifty['type'] 			= 1;
				$mnifty['r_status'] 		= 0;
				$mnifty['uploaded_no'] 		= $uploaded_no1;
				$mnifty['created_at'] 		= date("Y-m-d H:i:s", strtotime($timestamp));

				$this->welcome_model->insert_PE($mnifty);
			}

		}

	}

	function save_banknifty()
	{
		$this->load->library('curl');
		
		$api_url = "https://www.nseindia.com/api/option-chain-indices?symbol=BANKNIFTY";

        $ch = curl_init();
    	curl_setopt($ch, CURLOPT_URL, $api_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
   		curl_close($ch);

		$nifty_json 		= json_decode($output);
		if(empty($nifty_json->records))
		{
			die('Bank nifty no data'); exit();
		}

		$records 			= $nifty_json->records;
		$expiryDates		= $records->expiryDates;

		if(!empty($expiryDates))
		{		
			foreach($expiryDates as $exDate)
			{
				$ex_res = $this->welcome_model->check_expiry_date($exDate);
				if(empty($ex_res))
				{
					$expiry['expiry_date'] = date("Y-m-d",strtotime($exDate));
					$this->welcome_model->insert_expiry_date($expiry);
				}
			}		
		}
		
		$data 				= $records->data;
		$timestamp 			= $records->timestamp;
		$underlyingValue	= $records->underlyingValue;

		$uploaded_no		= $this->welcome_model->get_max_uploaded_no();
		$uploaded_no1     	= 1;
		if(!empty($uploaded_no['uploaded_no']))
		{
			$uploaded_no1 = $uploaded_no['uploaded_no'] + 1;
		}

		$snifty['type'] 				= 2;
		$snifty['r_status'] 			= 1;
		$snifty['uploaded_no'] 			= $uploaded_no1;
		$snifty['stock_price'] 			= $underlyingValue;
		$snifty['created_at'] 			= date("Y-m-d H:i:s", strtotime($timestamp));
		$this->welcome_model->insert_single_data($snifty);

		if(!empty($data))
		{

			foreach($data as $row_data)
			{

				$strikePrice 	= $row_data->strikePrice;
				$expiryDate 	= $row_data->expiryDate;

				if(!empty($row_data->CE))
				{
					$CE = $row_data->CE;
					
					$mnifty['calls_oi'] 		= $CE->openInterest;
					$mnifty['calls'] 			= $CE->changeinOpenInterest;
				}else{
					$mnifty['calls_oi'] 		= 0;
					$mnifty['calls'] 			= 0;
				}

				if(!empty($row_data->PE))
				{
					$PE = $row_data->PE;

					$mnifty['puts_oi'] 			= $PE->openInterest;
					$mnifty['puts'] 			= $PE->changeinOpenInterest;
					
				}else{
					$mnifty['puts_oi'] 			= 0;
					$mnifty['puts'] 			= 0;
				}

				$mnifty['stock_price'] 		= $strikePrice;
				$mnifty['expiry_date'] 		= date("Y-m-d", strtotime($expiryDate));
				$mnifty['type'] 			= 2;
				$mnifty['r_status'] 		= 0;
				$mnifty['uploaded_no'] 		= $uploaded_no1;
				$mnifty['created_at'] 		= date("Y-m-d H:i:s", strtotime($timestamp));

				$this->welcome_model->insert_PE($mnifty);
			}

		}

	}

	function export_nse_record()
	{

		$post_data = $this->input->post();

		echo print_r($post_data);exit;

		$expiry_date = $post_data['expiry_date'];

		if(empty($expiry_date))
		{
			echo "Date is required";exit;
		}

		$this->save_nifty();
		$this->save_banknifty();
		
		$this->load->helper('download');
		$this->load->library('excel');

        $filePath       = './uploads/nse.xlsx';
		$fileName       = 'nse.xlsx';
		$objPHPExcel 	= new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0);

		$fontstyle = array(
			'font'  => array(
				'color' => array('rgb' => 'FFFFFF'),
			)
		);

		$font_bold = array(
			'font'  => array(
				'bold' => true,
			)
		);

		foreach(range('A','G') as $columnID1) 
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID1)->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID1)->setWidth(10.00);
		}

		foreach(range('I','O') as $columnID2) 
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID2)->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID2)->setWidth(10.00);
		}

		//Nifty
		$type = 1;
		$uploaded_result = $this->welcome_model->get_uploaded_no($type);
		foreach($uploaded_result as $uploaded)
		{
			$u_uploaded_no1[]  = $uploaded['uploaded_no'];
		}
		rsort($u_uploaded_no1);

		if(!empty($u_uploaded_no1))
		{

			$rowCount1      = 1;

			foreach ($u_uploaded_no1 as $unique_no1)
			{

				$single_uploaded_result = $this->welcome_model->get_single_uploaded($unique_no1, $type);

				// echo "<pre>";
				// print_r($single_uploaded_result['stock_price']);exit;

				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount1, "NIFTY");
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount1, $single_uploaded_result['stock_price']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount1, date("g:i A",strtotime($single_uploaded_result['created_at'])));

				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('b4c7e7');			

				$rowCount1++;

				$objPHPExcel->getActiveSheet()->SetCellValue("A".$rowCount1, "CALL OI");

				$objPHPExcel->getActiveSheet()->SetCellValue("B".$rowCount1, "CALL OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("C".$rowCount1, "Strick Price");

				$objPHPExcel->getActiveSheet()->SetCellValue("D".$rowCount1, "PUT OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("E".$rowCount1, "PUT OI");       

				$objPHPExcel->getActiveSheet()->SetCellValue("F".$rowCount1, "PCR");

				$objPHPExcel->getActiveSheet()->SetCellValue("G".$rowCount1, "TREND");

				$rowCount1++;

				//echo $single_uploaded_result['stock_price'];exit;
				if(empty($single_uploaded_result['stock_price']))
				{
					continue;
				}

				$lessthen_stock_data 		= $this->welcome_model->get_lessthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				//echo $this->db->last_query();exit;

				$DOM[0]['id'] 				= '';
				$DOM[0]['type'] 			= '';
				$DOM[0]['calls'] 			= '';
				$DOM[0]['calls_oi'] 		= '';
				$DOM[0]['puts'] 			= '';
				$DOM[0]['puts_oi'] 			= '';
				$DOM[0]['stock_price']		= $single_uploaded_result['stock_price'];
				$DOM[0]['expiry_date']		= '';
				$DOM[0]['uploaded_no']		= '';
				$DOM[0]['r_status']			= $single_uploaded_result['r_status'];
				$DOM[0]['created_at']		= '';

				$greaterthen_stock_data 	= $this->welcome_model->get_greaterthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				$merge_array = array_merge($lessthen_stock_data, $DOM, $greaterthen_stock_data);

				$total_stock_price1      = 0;
				$total_puts_vl1          = 0;
				$total_calls_vl1         = 0;
				$total_puts_oi_val1      = 0;
				$total_calls_oi_val1     = 0;

				if(!empty($merge_array))
				{

					foreach($merge_array as $ls_row)
					{
						
						if($ls_row['stock_price'] != "")
						{
							$stock_price      = $ls_row['stock_price'];
							$stock_price_vl   = $ls_row['stock_price'];
						}else{
							$stock_price      = "";
							$stock_price_vl   = 0;
						}

						if($ls_row['calls'] != "")
						{
							$calls      = $ls_row['calls'];
							$calls_vl   = $ls_row['calls'];
						}else{
							$calls      = "";
							$calls_vl   = 0;
						}

						if($ls_row['calls_oi'] != "")
						{
							$calls_oi      = $ls_row['calls_oi'];
							$calls_oi_val  = $ls_row['calls_oi'];
						}else{
							$calls_oi      = "";
							$calls_oi_val  = 0;
						}

						if($ls_row['puts'] != "")
						{
							$puts      = $ls_row['puts'];
							$puts_vl   = $ls_row['puts'];
						}else{
							$puts      = "";
							$puts_vl   = 0;
						}

						if($ls_row['puts_oi'] != "")
						{
							$puts_oi      = $ls_row['puts_oi'];
							$puts_oi_val  = $ls_row['puts_oi'];
						}else{
							$puts_oi      = "";
							$puts_oi_val  = 0;
						}

						if($ls_row['r_status'] == 1)
						{
							$trend = "";
							$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($calls_vl > $puts_vl)
							{
								$trend = "SELL";
							}else{
								$trend = "BUY";
							}
						}

						$PCR1 = @($puts_oi_val / $calls_oi_val);
						$PCR1 = number_format((float)$PCR1, 2, '.', '');
						if($PCR1 == "nan" || $PCR1 == "inf")
						{
							$PCR1 = "";
						}

						$total_stock_price1      = $total_stock_price1 + $stock_price_vl;
						$total_puts_vl1          = $total_puts_vl1 + $puts_vl;
						$total_calls_vl1         = $total_calls_vl1 + $calls_vl;
						$total_puts_oi_val1      = $total_puts_oi_val1 + $puts_oi_val;
						$total_calls_oi_val1     = $total_calls_oi_val1 + $calls_oi_val;

						$objPHPExcel->getActiveSheet()->SetCellValue("A".$rowCount1, $calls_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("B".$rowCount1, $calls);
						if($calls < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("C".$rowCount1, $stock_price);

						$objPHPExcel->getActiveSheet()->SetCellValue("D".$rowCount1, $puts);
						if($puts < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("E".$rowCount1, $puts_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("F".$rowCount1, $PCR1);

						$objPHPExcel->getActiveSheet()->SetCellValue("G".$rowCount1, $trend);
						if($ls_row['r_status'] == 1)
						{
							$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($trend == "SELL")
							{
								$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
							}else{
								$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
							}
						}

						$rowCount1++;
					}

					if($total_calls_vl1 > $total_puts_vl1)
					{
						$trend = "SELL";
					}else{
						$trend = "BUY";
					}

					$TOTAL_PCR1 = @($total_puts_oi_val1 / $total_calls_oi_val1);
					$TOTAL_PCR1 = number_format((float)$TOTAL_PCR1, 2, '.', '');
					if($TOTAL_PCR1 == "nan" || $TOTAL_PCR1 == "inf")
					{
						$TOTAL_PCR1 = "";
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("A".$rowCount1, $total_calls_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("B".$rowCount1, $total_calls_vl1);
					if($total_calls_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("C".$rowCount1, $total_stock_price1);

					$objPHPExcel->getActiveSheet()->SetCellValue("D".$rowCount1, $total_puts_vl1);
					if($total_puts_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("E".$rowCount1, $total_puts_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("F".$rowCount1, $TOTAL_PCR1);

					$objPHPExcel->getActiveSheet()->SetCellValue("G".$rowCount1, $trend);
					if($trend == "SELL")
					{
						$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
					}else{
						$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
					}

					$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':'.'G'.$rowCount1)->applyFromArray($font_bold);

					$rowCount1 = $rowCount1 + 2;
				}

			}

		}


		//Bank Nifty
		$type = 2;
		$uploaded_result = $this->welcome_model->get_uploaded_no($type);
		foreach($uploaded_result as $uploaded)
		{
			$u_uploaded_no2[]  = $uploaded['uploaded_no'];
		}
		rsort($u_uploaded_no2);

		if(!empty($u_uploaded_no2))
		{

			$rowCount1      = 1;

			foreach ($u_uploaded_no2 as $unique_no1)
			{

				$single_uploaded_result = $this->welcome_model->get_single_uploaded($unique_no1, $type);

				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount1, "BANK NIFTY");
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount1, $single_uploaded_result['stock_price']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount1, date("g:i A",strtotime($single_uploaded_result['created_at'])));

				$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('b4c7e7');			

				$rowCount1++;

				$objPHPExcel->getActiveSheet()->SetCellValue("I".$rowCount1, "CALL OI");

				$objPHPExcel->getActiveSheet()->SetCellValue("J".$rowCount1, "CALL OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("K".$rowCount1, "Strick Price");

				$objPHPExcel->getActiveSheet()->SetCellValue("L".$rowCount1, "PUT OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("M".$rowCount1, "PUT OI");       

				$objPHPExcel->getActiveSheet()->SetCellValue("N".$rowCount1, "PCR");

				$objPHPExcel->getActiveSheet()->SetCellValue("O".$rowCount1, "TREND");

				$rowCount1++;

				$lessthen_stock_data 		= $this->welcome_model->get_lessthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				$DOM[0]['id'] 				= '';
				$DOM[0]['type'] 			= '';
				$DOM[0]['calls'] 			= '';
				$DOM[0]['calls_oi'] 		= '';
				$DOM[0]['puts'] 			= '';
				$DOM[0]['puts_oi'] 			= '';
				$DOM[0]['stock_price']		= $single_uploaded_result['stock_price'];
				$DOM[0]['expiry_date']		= '';
				$DOM[0]['uploaded_no']		= '';
				$DOM[0]['r_status']			= $single_uploaded_result['r_status'];
				$DOM[0]['created_at']		= '';

				$greaterthen_stock_data 	= $this->welcome_model->get_greaterthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				$merge_array = array_merge($lessthen_stock_data, $DOM, $greaterthen_stock_data);

				$total_stock_price1      = 0;
				$total_puts_vl1          = 0;
				$total_calls_vl1         = 0;
				$total_puts_oi_val1      = 0;
				$total_calls_oi_val1     = 0;

				if(!empty($merge_array))
				{

					foreach($merge_array as $ls_row)
					{
						
						if($ls_row['stock_price'] != "")
						{
							$stock_price      = $ls_row['stock_price'];
							$stock_price_vl   = $ls_row['stock_price'];
						}else{
							$stock_price      = "";
							$stock_price_vl   = 0;
						}

						if($ls_row['calls'] != "")
						{
							$calls      = $ls_row['calls'];
							$calls_vl   = $ls_row['calls'];
						}else{
							$calls      = "";
							$calls_vl   = 0;
						}

						if($ls_row['calls_oi'] != "")
						{
							$calls_oi      = $ls_row['calls_oi'];
							$calls_oi_val  = $ls_row['calls_oi'];
						}else{
							$calls_oi      = "";
							$calls_oi_val  = 0;
						}

						if($ls_row['puts'] != "")
						{
							$puts      = $ls_row['puts'];
							$puts_vl   = $ls_row['puts'];
						}else{
							$puts      = "";
							$puts_vl   = 0;
						}

						if($ls_row['puts_oi'] != "")
						{
							$puts_oi      = $ls_row['puts_oi'];
							$puts_oi_val  = $ls_row['puts_oi'];
						}else{
							$puts_oi      = "";
							$puts_oi_val  = 0;
						}

						if($ls_row['r_status'] == 1)
						{
							$trend = "";
							$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($calls_vl > $puts_vl)
							{
								$trend = "SELL";
							}else{
								$trend = "BUY";
							}
						}

						$PCR1 = @($puts_oi_val / $calls_oi_val);
						$PCR1 = number_format((float)$PCR1, 2, '.', '');
						if($PCR1 == "nan" || $PCR1 == "inf")
						{
							$PCR1 = "";
						}

						$total_stock_price1      = $total_stock_price1 + $stock_price_vl;
						$total_puts_vl1          = $total_puts_vl1 + $puts_vl;
						$total_calls_vl1         = $total_calls_vl1 + $calls_vl;
						$total_puts_oi_val1      = $total_puts_oi_val1 + $puts_oi_val;
						$total_calls_oi_val1     = $total_calls_oi_val1 + $calls_oi_val;

						$objPHPExcel->getActiveSheet()->SetCellValue("I".$rowCount1, $calls_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("J".$rowCount1, $calls);
						if($calls < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("K".$rowCount1, $stock_price);

						$objPHPExcel->getActiveSheet()->SetCellValue("L".$rowCount1, $puts);
						if($puts < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("M".$rowCount1, $puts_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("N".$rowCount1, $PCR1);

						$objPHPExcel->getActiveSheet()->SetCellValue("O".$rowCount1, $trend);
						if($ls_row['r_status'] == 1)
						{
							$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($trend == "SELL")
							{
								$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
							}else{
								$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
							}
						}

						$rowCount1++;
					}

					if($total_calls_vl1 > $total_puts_vl1)
					{
						$trend = "SELL";
					}else{
						$trend = "BUY";
					}

					$TOTAL_PCR1 = @($total_puts_oi_val1 / $total_calls_oi_val1);
					$TOTAL_PCR1 = number_format((float)$TOTAL_PCR1, 2, '.', '');
					if($TOTAL_PCR1 == "nan" || $TOTAL_PCR1 == "inf")
					{
						$TOTAL_PCR1 = "";
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("I".$rowCount1, $total_calls_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("J".$rowCount1, $total_calls_vl1);
					if($total_calls_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("K".$rowCount1, $total_stock_price1);

					$objPHPExcel->getActiveSheet()->SetCellValue("L".$rowCount1, $total_puts_vl1);
					if($total_puts_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("M".$rowCount1, $total_puts_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("N".$rowCount1, $TOTAL_PCR1);

					$objPHPExcel->getActiveSheet()->SetCellValue("O".$rowCount1, $trend);
					if($trend == "SELL")
					{
						$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
					}else{
						$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
					}

					$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':'.'O'.$rowCount1)->applyFromArray($font_bold);

				}

			}

		}

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($filePath);

		$file_arr = ['filePath' => $filePath, 'fileName' => $fileName];

		// download file
		//$MainPath = file_get_contents($file_arr['filePath']);
		//force_download($file_arr['filePath'], $MainPath);

		// // // download file
		// // $MainPath = file_get_contents($file_arr['filePath']);
		// // force_download($file_arr['filePath'], $MainPath);

		// download file
		$filePath = file_get_contents($file_arr['filePath']);

		$report_url = "./uploads/".$file_arr['fileName'];

		$output = array(
			"report_url"   => $report_url,
		);

		echo json_encode($output); exit;
	}

	function download_nse_record($expiry_date)
	{
		if(empty($expiry_date))
		{
			echo "Date is required";exit;
		}

		$this->save_nifty();
		$this->save_banknifty();
		
		$this->load->helper('download');
		$this->load->library('excel');

        $filePath       = './uploads/nse.xlsx';
		$fileName       = 'nse.xlsx';
		$objPHPExcel 	= new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0);

		$fontstyle = array(
			'font'  => array(
				'color' => array('rgb' => 'FFFFFF'),
			)
		);

		$font_bold = array(
			'font'  => array(
				'bold' => true,
			)
		);

		foreach(range('A','G') as $columnID1) 
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID1)->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID1)->setWidth(10.00);
		}

		foreach(range('I','O') as $columnID2) 
		{
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID2)->setAutoSize(false);
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID2)->setWidth(10.00);
		}

		//Nifty
		$type = 1;
		$uploaded_result = $this->welcome_model->get_uploaded_no($type);
		foreach($uploaded_result as $uploaded)
		{
			$u_uploaded_no1[]  = $uploaded['uploaded_no'];
		}
		rsort($u_uploaded_no1);

		if(!empty($u_uploaded_no1))
		{

			$rowCount1      = 1;

			foreach ($u_uploaded_no1 as $unique_no1)
			{

				$single_uploaded_result = $this->welcome_model->get_single_uploaded($unique_no1, $type);

				// echo "<pre>";
				// print_r($single_uploaded_result['stock_price']);exit;

				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount1, "NIFTY");
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount1, $single_uploaded_result['stock_price']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount1, date("g:i A",strtotime($single_uploaded_result['created_at'])));

				$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('b4c7e7');			

				$rowCount1++;

				$objPHPExcel->getActiveSheet()->SetCellValue("A".$rowCount1, "CALL OI");

				$objPHPExcel->getActiveSheet()->SetCellValue("B".$rowCount1, "CALL OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("C".$rowCount1, "Strick Price");

				$objPHPExcel->getActiveSheet()->SetCellValue("D".$rowCount1, "PUT OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("E".$rowCount1, "PUT OI");       

				$objPHPExcel->getActiveSheet()->SetCellValue("F".$rowCount1, "PCR");

				$objPHPExcel->getActiveSheet()->SetCellValue("G".$rowCount1, "TREND");

				$rowCount1++;

				//echo $single_uploaded_result['stock_price'];exit;
				if(empty($single_uploaded_result['stock_price']))
				{
					continue;
				}

				$lessthen_stock_data 		= $this->welcome_model->get_lessthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				//echo $this->db->last_query();exit;

				$DOM[0]['id'] 				= '';
				$DOM[0]['type'] 			= '';
				$DOM[0]['calls'] 			= '';
				$DOM[0]['calls_oi'] 		= '';
				$DOM[0]['puts'] 			= '';
				$DOM[0]['puts_oi'] 			= '';
				$DOM[0]['stock_price']		= $single_uploaded_result['stock_price'];
				$DOM[0]['expiry_date']		= '';
				$DOM[0]['uploaded_no']		= '';
				$DOM[0]['r_status']			= $single_uploaded_result['r_status'];
				$DOM[0]['created_at']		= '';

				$greaterthen_stock_data 	= $this->welcome_model->get_greaterthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				$merge_array = array_merge($lessthen_stock_data, $DOM, $greaterthen_stock_data);

				$total_stock_price1      = 0;
				$total_puts_vl1          = 0;
				$total_calls_vl1         = 0;
				$total_puts_oi_val1      = 0;
				$total_calls_oi_val1     = 0;

				if(!empty($merge_array))
				{

					foreach($merge_array as $ls_row)
					{
						
						if($ls_row['stock_price'] != "")
						{
							$stock_price      = $ls_row['stock_price'];
							$stock_price_vl   = $ls_row['stock_price'];
						}else{
							$stock_price      = "";
							$stock_price_vl   = 0;
						}

						if($ls_row['calls'] != "")
						{
							$calls      = $ls_row['calls'];
							$calls_vl   = $ls_row['calls'];
						}else{
							$calls      = "";
							$calls_vl   = 0;
						}

						if($ls_row['calls_oi'] != "")
						{
							$calls_oi      = $ls_row['calls_oi'];
							$calls_oi_val  = $ls_row['calls_oi'];
						}else{
							$calls_oi      = "";
							$calls_oi_val  = 0;
						}

						if($ls_row['puts'] != "")
						{
							$puts      = $ls_row['puts'];
							$puts_vl   = $ls_row['puts'];
						}else{
							$puts      = "";
							$puts_vl   = 0;
						}

						if($ls_row['puts_oi'] != "")
						{
							$puts_oi      = $ls_row['puts_oi'];
							$puts_oi_val  = $ls_row['puts_oi'];
						}else{
							$puts_oi      = "";
							$puts_oi_val  = 0;
						}

						if($ls_row['r_status'] == 1)
						{
							$trend = "";
							$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($calls_vl > $puts_vl)
							{
								$trend = "SELL";
							}else{
								$trend = "BUY";
							}
						}

						$PCR1 = @($puts_oi_val / $calls_oi_val);
						$PCR1 = number_format((float)$PCR1, 2, '.', '');
						if($PCR1 == "nan" || $PCR1 == "inf")
						{
							$PCR1 = "";
						}

						$total_stock_price1      = $total_stock_price1 + $stock_price_vl;
						$total_puts_vl1          = $total_puts_vl1 + $puts_vl;
						$total_calls_vl1         = $total_calls_vl1 + $calls_vl;
						$total_puts_oi_val1      = $total_puts_oi_val1 + $puts_oi_val;
						$total_calls_oi_val1     = $total_calls_oi_val1 + $calls_oi_val;

						$objPHPExcel->getActiveSheet()->SetCellValue("A".$rowCount1, $calls_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("B".$rowCount1, $calls);
						if($calls < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("C".$rowCount1, $stock_price);

						$objPHPExcel->getActiveSheet()->SetCellValue("D".$rowCount1, $puts);
						if($puts < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("E".$rowCount1, $puts_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("F".$rowCount1, $PCR1);

						$objPHPExcel->getActiveSheet()->SetCellValue("G".$rowCount1, $trend);
						if($ls_row['r_status'] == 1)
						{
							$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($trend == "SELL")
							{
								$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
							}else{
								$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
							}
						}

						$rowCount1++;
					}

					if($total_calls_vl1 > $total_puts_vl1)
					{
						$trend = "SELL";
					}else{
						$trend = "BUY";
					}

					$TOTAL_PCR1 = @($total_puts_oi_val1 / $total_calls_oi_val1);
					$TOTAL_PCR1 = number_format((float)$TOTAL_PCR1, 2, '.', '');
					if($TOTAL_PCR1 == "nan" || $TOTAL_PCR1 == "inf")
					{
						$TOTAL_PCR1 = "";
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("A".$rowCount1, $total_calls_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("B".$rowCount1, $total_calls_vl1);
					if($total_calls_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('B'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("C".$rowCount1, $total_stock_price1);

					$objPHPExcel->getActiveSheet()->SetCellValue("D".$rowCount1, $total_puts_vl1);
					if($total_puts_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('D'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("E".$rowCount1, $total_puts_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("F".$rowCount1, $TOTAL_PCR1);

					$objPHPExcel->getActiveSheet()->SetCellValue("G".$rowCount1, $trend);
					if($trend == "SELL")
					{
						$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
					}else{
						$objPHPExcel->getActiveSheet()->getStyle('G'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
					}

					$objPHPExcel->getActiveSheet()->getStyle('A'.$rowCount1.':'.'G'.$rowCount1)->applyFromArray($font_bold);

					$rowCount1 = $rowCount1 + 2;
				}

			}

		}


		//Bank Nifty
		$type = 2;
		$uploaded_result = $this->welcome_model->get_uploaded_no($type);
		foreach($uploaded_result as $uploaded)
		{
			$u_uploaded_no2[]  = $uploaded['uploaded_no'];
		}
		rsort($u_uploaded_no2);

		if(!empty($u_uploaded_no2))
		{

			$rowCount1      = 1;

			foreach ($u_uploaded_no2 as $unique_no1)
			{

				$single_uploaded_result = $this->welcome_model->get_single_uploaded($unique_no1, $type);

				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount1, "BANK NIFTY");
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount1, $single_uploaded_result['stock_price']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount1, date("g:i A",strtotime($single_uploaded_result['created_at'])));

				$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('b4c7e7');			

				$rowCount1++;

				$objPHPExcel->getActiveSheet()->SetCellValue("I".$rowCount1, "CALL OI");

				$objPHPExcel->getActiveSheet()->SetCellValue("J".$rowCount1, "CALL OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("K".$rowCount1, "Strick Price");

				$objPHPExcel->getActiveSheet()->SetCellValue("L".$rowCount1, "PUT OI CHANGE");

				$objPHPExcel->getActiveSheet()->SetCellValue("M".$rowCount1, "PUT OI");       

				$objPHPExcel->getActiveSheet()->SetCellValue("N".$rowCount1, "PCR");

				$objPHPExcel->getActiveSheet()->SetCellValue("O".$rowCount1, "TREND");

				$rowCount1++;

				$lessthen_stock_data 		= $this->welcome_model->get_lessthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				$DOM[0]['id'] 				= '';
				$DOM[0]['type'] 			= '';
				$DOM[0]['calls'] 			= '';
				$DOM[0]['calls_oi'] 		= '';
				$DOM[0]['puts'] 			= '';
				$DOM[0]['puts_oi'] 			= '';
				$DOM[0]['stock_price']		= $single_uploaded_result['stock_price'];
				$DOM[0]['expiry_date']		= '';
				$DOM[0]['uploaded_no']		= '';
				$DOM[0]['r_status']			= $single_uploaded_result['r_status'];
				$DOM[0]['created_at']		= '';

				$greaterthen_stock_data 	= $this->welcome_model->get_greaterthen_stock_record($unique_no1, $single_uploaded_result['stock_price'], $expiry_date, $type);

				$merge_array = array_merge($lessthen_stock_data, $DOM, $greaterthen_stock_data);

				$total_stock_price1      = 0;
				$total_puts_vl1          = 0;
				$total_calls_vl1         = 0;
				$total_puts_oi_val1      = 0;
				$total_calls_oi_val1     = 0;

				if(!empty($merge_array))
				{

					foreach($merge_array as $ls_row)
					{
						
						if($ls_row['stock_price'] != "")
						{
							$stock_price      = $ls_row['stock_price'];
							$stock_price_vl   = $ls_row['stock_price'];
						}else{
							$stock_price      = "";
							$stock_price_vl   = 0;
						}

						if($ls_row['calls'] != "")
						{
							$calls      = $ls_row['calls'];
							$calls_vl   = $ls_row['calls'];
						}else{
							$calls      = "";
							$calls_vl   = 0;
						}

						if($ls_row['calls_oi'] != "")
						{
							$calls_oi      = $ls_row['calls_oi'];
							$calls_oi_val  = $ls_row['calls_oi'];
						}else{
							$calls_oi      = "";
							$calls_oi_val  = 0;
						}

						if($ls_row['puts'] != "")
						{
							$puts      = $ls_row['puts'];
							$puts_vl   = $ls_row['puts'];
						}else{
							$puts      = "";
							$puts_vl   = 0;
						}

						if($ls_row['puts_oi'] != "")
						{
							$puts_oi      = $ls_row['puts_oi'];
							$puts_oi_val  = $ls_row['puts_oi'];
						}else{
							$puts_oi      = "";
							$puts_oi_val  = 0;
						}

						if($ls_row['r_status'] == 1)
						{
							$trend = "";
							$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($calls_vl > $puts_vl)
							{
								$trend = "SELL";
							}else{
								$trend = "BUY";
							}
						}

						$PCR1 = @($puts_oi_val / $calls_oi_val);
						$PCR1 = number_format((float)$PCR1, 2, '.', '');
						if($PCR1 == "nan" || $PCR1 == "inf")
						{
							$PCR1 = "";
						}

						$total_stock_price1      = $total_stock_price1 + $stock_price_vl;
						$total_puts_vl1          = $total_puts_vl1 + $puts_vl;
						$total_calls_vl1         = $total_calls_vl1 + $calls_vl;
						$total_puts_oi_val1      = $total_puts_oi_val1 + $puts_oi_val;
						$total_calls_oi_val1     = $total_calls_oi_val1 + $calls_oi_val;

						$objPHPExcel->getActiveSheet()->SetCellValue("I".$rowCount1, $calls_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("J".$rowCount1, $calls);
						if($calls < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("K".$rowCount1, $stock_price);

						$objPHPExcel->getActiveSheet()->SetCellValue("L".$rowCount1, $puts);
						if($puts < 0)
						{
							$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

							$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->applyFromArray($fontstyle);
						}

						$objPHPExcel->getActiveSheet()->SetCellValue("M".$rowCount1, $puts_oi);

						$objPHPExcel->getActiveSheet()->SetCellValue("N".$rowCount1, $PCR1);

						$objPHPExcel->getActiveSheet()->SetCellValue("O".$rowCount1, $trend);
						if($ls_row['r_status'] == 1)
						{
							$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('D3D3D3');
						}else{
							if($trend == "SELL")
							{
								$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
							}else{
								$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
							}
						}

						$rowCount1++;
					}

					if($total_calls_vl1 > $total_puts_vl1)
					{
						$trend = "SELL";
					}else{
						$trend = "BUY";
					}

					$TOTAL_PCR1 = @($total_puts_oi_val1 / $total_calls_oi_val1);
					$TOTAL_PCR1 = number_format((float)$TOTAL_PCR1, 2, '.', '');
					if($TOTAL_PCR1 == "nan" || $TOTAL_PCR1 == "inf")
					{
						$TOTAL_PCR1 = "";
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("I".$rowCount1, $total_calls_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("J".$rowCount1, $total_calls_vl1);
					if($total_calls_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('J'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("K".$rowCount1, $total_stock_price1);

					$objPHPExcel->getActiveSheet()->SetCellValue("L".$rowCount1, $total_puts_vl1);
					if($total_puts_vl1 < 0)
					{
						$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('8B0000');

						$objPHPExcel->getActiveSheet()->getStyle('L'.$rowCount1)->applyFromArray($fontstyle);
					}

					$objPHPExcel->getActiveSheet()->SetCellValue("M".$rowCount1, $total_puts_oi_val1);

					$objPHPExcel->getActiveSheet()->SetCellValue("N".$rowCount1, $TOTAL_PCR1);

					$objPHPExcel->getActiveSheet()->SetCellValue("O".$rowCount1, $trend);
					if($trend == "SELL")
					{
						$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('ffc7ce');
					}else{
						$objPHPExcel->getActiveSheet()->getStyle('O'.$rowCount1)->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setARGB('c6efce');
					}

					$objPHPExcel->getActiveSheet()->getStyle('I'.$rowCount1.':'.'O'.$rowCount1)->applyFromArray($font_bold);

				}

			}

		}

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
		$objWriter->save($filePath);

		$file_arr = ['filePath' => $filePath, 'fileName' => $fileName];

		// download file
		$MainPath = file_get_contents($file_arr['filePath']);
		force_download($file_arr['filePath'], $MainPath);
	}
}
