<!DOCTYPE html>
<html lang="en">

<head>

  <title>NSE</title>

  <!-- Vendor CSS Files -->
  <!--link href="<?= base_url(); ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"-->

  <style>
    .pointer-events{
        pointer-events: none;
    }
  </style>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body id="body">

  <section id="hero-animated" class="hero-animated d-flex align-items-center">
  
    <div class="container d-flex flex-column justify-content-center align-items-center text-center position-relative" data-aos="zoom-out">
      <div class="d-flex">
        <select class="form-control" id="expiry_date" name="expiry_date">
          <!--option value="">---Select---</option-->
          <?php if(!empty($expiry_date)): foreach($expiry_date as $ex_date): ?>
            <option value="<?= $ex_date['expiry_date']; ?>"><?= date("d-M-Y",strtotime($ex_date['expiry_date'])) ?></option>
          <?php endforeach; endif; ?>
        </select>
      </div>
      <br/>
      <div class="d-flex">
          <button type="button" id="download" class="btn btn-primary">Export</button>
          <button type="button" id="downloading" class="btn btn-success" style="display:none;" disabled><i class="fa fa-refresh fa-spin"></i> Loading..</button>
      </div>
      
    </div>
  </section>

  <!-- Template Main JS File -->
  <!--script src="<?= base_url(); ?>assets/js/main.js"></script-->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  
  <script>
  $(".numeric-only").bind("change keyup input", function() 
  {
      var position = this.selectionStart - 1;
      //remove all but number and .
      var fixed = this.value.replace(/[^0-9]/gi, "");
      if (fixed.charAt(0) === ".")
      //can't start with .
      fixed = fixed.slice(1);

      var pos = fixed.indexOf(".") + 1;
      if (pos >= 0)
      //avoid more than one .
      fixed = fixed.substr(0, pos) + fixed.slice(pos).replace(".", "");

      if (this.value !== fixed) {
      this.value = fixed;
      this.selectionStart = position;
      this.selectionEnd = position;
      }
  });

  $(document).ready(function(){

    $('#stock_upload').on("submit", function(e){
        e.preventDefault(); //form will not submitted

        $.ajax({
            url:"import.php",
            method:"POST",
            data:new FormData(this),
            contentType:false,          // The content type used when sending data to the server.
            cache:false,                // To unable request pages to be cached
            processData:false,          // To send DOMDocument or non processed data file it is set to false
            success: function(data){
                var res = $.parseJSON(data);
                if(res.status == "success")
                {
                    $(".alert").fadeIn('5000', function() {
                        $(".alert").css("display","block");
                        $(".alert").addClass("alert-success");
                        $(".alert").removeClass("alert-danger");
                        $(".alert").text(res.message);
                    });

                    $('#nifty_price').val('');
                    $('#bank_nifty_price').val('');
                    $('#nifty_csv_file').val('');
                    $('#bank_nifty_csv_file').val('');
                }else{

                    $(".alert").fadeIn('5000', function() {
                        $(".alert").css("display","block");
                        $(".alert").addClass("alert-danger");
                        $(".alert").removeClass("alert-success");
                        $(".alert").text(res.message);
                    });
                }
            }
        });

    });  
});

$('#download').on("click", function(e){
    e.preventDefault(); //form will not submitted

    var expiry_date = $("#expiry_date").val();
    if(!expiry_date)
    {
      alert("Please select date")
      return false;
    }

    // $("#body").addClass('pointer-events');
    // $("#download").hide();
    // $("#downloading").show();

    $.ajax({
        url:"<?= base_url() ?>welcome/export_nse_record",
        method:"POST",
        data: {expiry_date : expiry_date},
        //contentType:false,          // The content type used when sending data to the server.
        //cache:false,                // To unable request pages to be cached
        //processData:false,          // To send DOMDocument or non processed data file it is set to false
        success: function(data){
            var res = $.parseJSON(data);

            // $("#body").removeClass('pointer-events');
            // $("#download").show();
            // $("#downloading").hide();

            location.href = res.report_url;
        }
    });

});

$('#reset').on("click", function(e){
    e.preventDefault(); //form will not submitted

    if (window.confirm("Are you sure?")) 
    {

        $.ajax({
            url:"reset.php",
            method:"POST",                       
            contentType:false,          // The content type used when sending data to the server.
            cache:false,                // To unable request pages to be cached
            processData:false,          // To send DOMDocument or non processed data file it is set to false
            success: function(data){
                var res = $.parseJSON(data);
                if(res.status == "success")
                {
                    $(".alert").css("display","block");
                    $(".alert").addClass("alert-danger");
                    $(".alert").text(res.message);

                    $('#nifty_price').val('');
                    $('#bank_nifty_price').val('');
                    $('#nifty_csv_file').val('');
                    $('#bank_nifty_csv_file').val('');
                }else{

                    $(".alert").css("display","block");
                    $(".alert").addClass("alert-danger");
                    $(".alert").text(res.message);
                }
            }
        });

    }

});
</script>

</body>

</html>