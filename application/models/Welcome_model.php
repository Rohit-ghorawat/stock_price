<?php 


class Welcome_Model extends CI_Model
{


    function insert_expiry_date($data)
    {
        $this->db->insert("st_expiry_date",$data);
        return $this->db->insert_id();
    }

    function insert_single_data($data)
    {
        $this->db->insert("st_price",$data);
        return $this->db->insert_id();
    }

    function insert_PE($data)
    {
        $this->db->insert("st_price",$data);
        return $this->db->insert_id();
    }

    function get_max_uploaded_no()
    {
        $this->db->select("MAX(uploaded_no) as uploaded_no");
        $this->db->from("st_price");

        $this->db->limit(1);

        $Query =  $this->db->get();
        return $Query->row_array();
    }

    function get_uploaded_no($type)
    {
        $this->db->select("uploaded_no");
        $this->db->from("st_price");
        $this->db->where("type", $type);

        $this->db->group_by("uploaded_no");

        $Query =  $this->db->get();
        return $Query->result_array();
    }

    function get_single_uploaded($unique_no1, $type)
    {
        $this->db->select("stock_price, r_status, created_at");
        $this->db->from("st_price");
        $this->db->where("uploaded_no", $unique_no1);
        $this->db->where("type", $type);
        $this->db->where("r_status", 1);

        $Query =  $this->db->get();
        return $Query->row_array();
    }

    function get_lessthen_stock_record($unique_no1, $stock_price, $expiry_date, $type)
    {

        if(empty($stock_price))
        {
            return FALSE;
        }

        $this->db->select("*");
        $this->db->from("st_price");
        $this->db->where("uploaded_no", $unique_no1);
        $this->db->where("type", $type);
        $this->db->where("stock_price <", $stock_price);
        $this->db->where("expiry_date", date("Y-m-d",strtotime($expiry_date)));

        $this->db->order_by("stock_price", 'DESC');
        $this->db->limit(10);

        $Query =  $this->db->get();
        return $Query->result_array();
    }

    function get_greaterthen_stock_record($unique_no1, $stock_price, $expiry_date, $type)
    {
        $this->db->select("*");
        $this->db->from("st_price");
        $this->db->where("uploaded_no", $unique_no1);
        $this->db->where("type", $type);
        $this->db->where("stock_price >=", $stock_price);
        $this->db->where("expiry_date", date("Y-m-d",strtotime($expiry_date)));

        $this->db->order_by("stock_price", 'ASC');
        $this->db->limit(10);

        $Query =  $this->db->get();
        return $Query->result_array();
    }

    function check_expiry_date($exDate)
    {
        $this->db->select("id");
        $this->db->from('st_expiry_date');
        $this->db->where('expiry_date',date("Y-m-d",strtotime($exDate)));

        return $this->db->count_all_results();
    }

    function get_expiry_date()
    {
        $this->db->select("id,expiry_date");
        $this->db->from('st_expiry_date');

        $Query =  $this->db->get();
        return $Query->result_array();        
    }

}